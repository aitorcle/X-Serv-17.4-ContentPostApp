import webapp

form = """
    <h2>Nueva direccion:</h2>
    <form action= " " method="POST"> 
    <input type="text" name="content">
    <input type="submit" value="Enviar"></form>
"""


class contentApp(webapp.webApp):
    dicc = {'/': " Pagina principal",
            '/hola': " Hola",
            '/adios': "Adios",
            'error': " Esa ruta no esta disponible "
            }

    def parse(self, request):  # Lee lo que le llega del navegador y extrae lo util

        meth = request.split(' ', 1)[0]
        res = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n', 2)[-1]

        return meth, res, body

    def process(self, parsedRequest):  # Coge los datos que ha sacado parse y lo utiliza

        meth, res, body = parsedRequest

        if meth == "GET":
            httpCode = "200 OK"
            print(res)
            if res in self.dicc.keys():
                print("Esta en el dic")
                htmlBody = "<html><body>El contenido es: " + self.dicc[res] \
                           + form + "</body></html>"

            else:
                print("No esta en el dic")
                htmlBody = "<html><body>Error, direccion no encontrada" + form + "</body></html>"

        elif meth == "PUT":
            httpCode = "200 OK"
            self.dicc[res] = body
            htmlBody = "<html><body><h1>" + "<br>" + self.dicc[res] + "</h1></body></html>"

        elif meth == "POST":
            cuerpost = body.split("\n")[-1]
            if cuerpost[0:7] == "content":
                res = "/" + body.split("=")[-1]
                self.dicc[res] = body.split("=")[-1]


            httpCode = "200 OK"
            htmlBody = "<html><body>Nuevo valor: " + self.dicc[res] \
                       + "<br><br>Al diccionario</body></html>"

        return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)


